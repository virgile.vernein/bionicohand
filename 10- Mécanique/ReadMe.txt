======================================================================================================
Historique des Versions
Main Bionicohand V2 Juillet 2023 -- > ENS-MAIN_BIONICO-A01		-- Solidworks 2022 Education
Main Bionicohand V2Beta Décembre 2023 -- > ENS-MAIN_BIONICO-B01		-- Solidworks 2022 Education


======================================================================================================

Mises en plans []:

Le dossier 03- MisesEnPlans_V2Beta contient les plans (CAO 2D et 3D).
Le fichier 2023_12_12_Plans_V2Beta.zip facilite le téléchargement de ces plans.