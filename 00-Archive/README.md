# Archive

## Contenu du dossier :

- [ ] [2021](https://gitlab.com/bionico/bionicohand/-/tree/main/Archive/2021) : Travaux effectués durant l'année 2021

- [ ] [2022](https://gitlab.com/bionico/bionicohand/-/tree/main/Archive/2022/Juillet%202022) : Travaux effectués durant l'année 2022

- [ ] [Arborescence_GitLab_de_l_archive](https://gitlab.com/bionico/bionicohand/-/blob/main/Archive/Arborescence_GitLab_de_l_archive.pdf) : Fichier PDF décrivant l'organisation de l'archive